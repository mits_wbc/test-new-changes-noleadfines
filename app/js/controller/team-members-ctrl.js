
app.controller('TeamMembersCtrl', ['$scope','$state','$timeout','allMembers','TeamMemberService',function($scope,$state,$timeout,allMembers,TeamMemberService) {
   $scope.$parent.deActiveLinks();
    $scope.$parent.isMemberListing = true;

  if(allMembers != null && allMembers.length >0){
    $scope.memberList  = allMembers;

  }

  $scope.getMemberSkillsContent = function(skills){
    var content ="<div class='row'><div class='col-lg-12 col-md-12 col-sm-12'><div class='box-tooltip'><h4 class='title'>Approved Skills Listing</h4><ul class='list-unstyled'>";

    angular.forEach(skills, function(skill, index){
      var skillClass = "";
      if(skill.skillPass == true ){
        skillClass="skill-passed";
      }

      content = content + "<li class=" + skillClass +  ">" + skill.skillDetail.name + "</li>";
    });
    content = content +'</ul></div></div></div>';
    return content;
  }

  $scope.deleteMember = function (memberId,memberName){

	    	swal({
	    		  title: 'Confirm',
	    		  text: "Do you want to delete the member "+memberName+"?",
	    		  type: 'warning',
	    		  showCancelButton: true,
	    		  cancelButtonText: 'Cancel',
	    		  confirmButtonText: 'Yes,Delete It!',
	    		  closeOnConfirm: false,
	    		  timer: 1000 * 60,
	    		}).then(function(isConfirm) {
                     if (isConfirm) {
                         TeamMemberService.delete(memberId).success(function(response){
                             swal(
                                  'Deleted!',
                                  'The member has been deleted.',
                                  'success'
                               );
                             $state.reload();
                         }).error(function(error){
                           $scope.errorList = error.errors;
                           var temp = Object.keys($scope.errorList);
                                 $scope.keys =  [];
                                 for(var i = 0; i < $scope.orderCustomKeys.length ; i++) {
                                       if(temp.indexOf($scope.orderCustomKeys[i])  != -1){
                                           $scope.keys.push($scope.orderCustomKeys[i]);
                                       }
                                 }
                         });
                    }
	    		});


  }

$timeout(function () {
function wheretoplace(){
    var width = window.innerWidth;
    if (width < 991) return 'bottom';
    return 'right';
}

    $(".nametooltip").popover({
            placement : wheretoplace,
            trigger : 'hover',
    		title: $(this).attr('data-content'),
            html: true,
        });
});

}]);
