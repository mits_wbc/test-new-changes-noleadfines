


/**
 * Application Controller
 */
app.controller('AppCtrl', ['$scope','$state','$filter','AuthenticationService', function ($scope,$state,$filter,AuthenticationService) {
  $scope.username = AuthenticationService.getName();
  $scope.logout = function(click){
    AuthenticationService.clearCache();
    $state.go('login', {}, {reload: true});
  }
    $scope.year = $filter('date')(new Date(),'yyyy');
    $scope.deActiveLinks = function(){
        $scope.isCompanyInfoPage = false;
        $scope.isMemberListing = false;
        $scope.isProjectListing = false;
        $scope.isReport = false;
    }

    $scope.gotoCompanyInformation = function(){
        $scope.deActiveLinks();
        $scope.isCompanyInfoPage = true;
        $state.go('app.company-information');
    }

    $scope.gotoMemberListing = function(){
         $scope.deActiveLinks();
         $scope.isMemberListing = true;
         $state.go('app.team-members');
    }

    $scope.gotoProjectListing = function(){
         $scope.deActiveLinks();
         $scope.isProjectListing = true;
         $state.go('app.projectList');
    }

    $scope.gotoReports = function(){
         $scope.deActiveLinks();
         $scope.isReport = true;
        $state.go('app.reports');
    }

}]);