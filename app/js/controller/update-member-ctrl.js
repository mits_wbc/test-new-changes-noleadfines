app.controller('UpdateMemberCtrl', ['$scope','$state','$anchorScroll','TeamMemberService','memberInfo',function($scope,$state,$anchorScroll,TeamMemberService,memberInfo) {

    $scope.member = {};
    $scope.isUpdate = false;
    $scope.orderCustomKeys = ['name','emailId','address1','userDTO','city','state','zipCode','homePhone'];
    if(memberInfo && memberInfo.data){
        $scope.isUpdate = true;
        $scope.member = memberInfo.data;
     }


 $scope.save = function(){
     $scope.member.notes = "N/A";
     $scope.member.isFromWebsite = true;
        TeamMemberService.saveOrUpdate($scope.member).success(function(response){
            $state.go('app.team-members', {}, {reload: true});
        }).error(function(error){
          $scope.errorList = error.errors;
          var temp = Object.keys($scope.errorList);
            $scope.keys =  [];
                for(var i = 0; i < $scope.orderCustomKeys.length ; i++) {
                  if(temp.indexOf($scope.orderCustomKeys[i])  != -1){
                      $scope.keys.push($scope.orderCustomKeys[i]);
                  }
            }
          $anchorScroll();
        });
   }

  $scope.cancle = function(){
    $scope.member = {};
     $state.go('app.team-members', {}, {reload: true});
  }

}]);