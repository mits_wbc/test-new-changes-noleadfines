app.controller('ForgotPasswordCtrl', function($scope,ChangePasswordService,$state,AuthenticationService,$rootScope) {

    $scope.backToLogin = function(){
      $state.go('login', {}, {reload: true});
    }

    $scope.user = {};
    AuthenticationService.clearCache();
    $scope.sendForgotRequest = function(){
      ChangePasswordService.sendForgotRequest($scope.user).success(function(response){
            $state.go('reset-password', {emailId:$scope.user.emailId}, {reload: true});
          }).error(function(error){
            $scope.errorList = error.errors;
            $scope.keys = Object.keys($scope.errorList);
            console.log("password error");

          });

    }

})
