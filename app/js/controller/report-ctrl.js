app.controller('ReportCtrl', ['$scope','reportData',function($scope,reportData) {

    $scope.myText = "My name is: <h1>John Doe</h1>";

    if(reportData){
        $scope.projects = reportData.data;
        $scope.companyInfo = reportData.data[0];
    }

}]);