
app.controller('CompanyInformationCtrl', ['$scope','CompanyService','companyInfo','$state','$anchorScroll',function($scope,CompanyService,companyInfo,$state,$anchorScroll) {
   $scope.$parent.deActiveLinks();
   $scope.$parent.isCompanyInfoPage = true;
   $scope.companyDTO = {};
   $scope.isUpdate = false;
   $scope.orderCustomKeys = ['firstName','firmName','lastName','companyDTO','companyEmail','companyAddress','companyCity','companyState','companyZipCode','officeNumber','emergencyContactFirstName','emergencyContactLastName','emergancyContactCellNumber'];

   if(companyInfo && companyInfo.data){
        $scope.isUpdate = true;

       $scope.companyDTO = companyInfo.data;
       $scope.companyDTO.renewalDate = new Date(companyInfo.data.renewalDate);
       $scope.companyDTO.submissionDate = new Date(companyInfo.data.submissionDate);
    }

    $scope.cancel = function() {
        if($scope.isUpdate){
            $state.go('app.team-members', {}, {reload: true});
        }else{
            $state.go('login', {}, {reload: true});
        }
    }

   $scope.save = function(){

    if($scope.companyDTO.id){
        CompanyService.saveOrUpdate($scope.companyDTO).success(function(response){
                   swal('<h3>You have successfully updated the company information</h3>')
                    $scope.errorList = null;
                    $scope.keys = null;
                }).error(function(error){
                  $scope.errorList = error.errors;
                   var temp = Object.keys($scope.errorList);
                         $scope.keys =  [];
                         for(var i = 0; i < $scope.orderCustomKeys.length ; i++) {
                               if(temp.indexOf($scope.orderCustomKeys[i]) != -1){
                                   $scope.keys.push($scope.orderCustomKeys[i]);
                               }
                         }
                   $anchorScroll();
                });

    }else{

         if(!$scope.companyDTO.renewalDate){
            $scope.companyDTO.renewalDate = new Date();
         }
         if(!$scope.companyDTO.submissionDate){
            $scope.companyDTO.submissionDate = new Date();
         }
        CompanyService.createCompany($scope.companyDTO).success(function(response){
                            swal(
                              '<h3>Thank you for registration with us.</h3>',
                              'You will shortly receive the login credential email to your company address.',
                              'success'
                            )
                             $scope.errorList = null;
                             $scope.keys = null;
                            $state.go('login', {}, {reload: true});
                        }).error(function(error){
                          $scope.errorList = error.errors;
                          var temp = Object.keys($scope.errorList);
                                $scope.keys =  [];
                                for(var i = 0; i < $scope.orderCustomKeys.length ; i++) {
                                      if(temp.indexOf($scope.orderCustomKeys[i]) != -1){
                                          $scope.keys.push($scope.orderCustomKeys[i]);
                                      }
                                }
                          $anchorScroll();
                        });
                   }

    }




}]);
