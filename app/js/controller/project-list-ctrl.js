app.controller('ProjectListCtrl', ['$scope', '$state', 'RrpProjectService', 'AuthenticationService','CompanyService', function($scope, $state, RrpProjectService, AuthenticationService,CompanyService) {
$scope.$parent.deActiveLinks();
$scope.$parent.isProjectListing = true;
  RrpProjectService.getProjects().then(function(data) {
    if (data.errors) {
      $scope.errorList = data.errors;
       $scope.keys = Object.keys($scope.errorList);
    } else {
      $scope.projectlist = data;
      $scope.errorList = null;
      $scope.keys = null;
    }
  });

  $scope.addProject = function(projectId){
    CompanyService.checkEPARegistration().success(function(data) {
         if(data.present){
            $state.go('app.RRP-project', {}, {reload: true});
         }else{
            $scope.$parent.deActiveLinks();
            $scope.$parent.isCompanyInfoPage = true;
            swal('<h3>Enter missing company certificates.</h3>')
            $state.go('app.company-information');
         }
     });

  }

  $scope.deleteProject = function(project) {
    /*RrpProjectService.delete(projectId);
     $state.go('app.projectList', {}, {reload: true});*/

    swal({
      title: 'Confirm',
      text: "Do you want to delete the project " + project.occupantName + " ?",
      type: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancel',
      confirmButtonText: 'Yes,Delete It!',
      closeOnConfirm: false,
      timer: 1000 * 60,
    }).then(function(isConfirm) {
      if (isConfirm) {
        RrpProjectService.delete(project.id).success(function(response) {
          swal(
            'Deleted!',
            'The Project has been deleted.',
            'success'
          );
          $state.reload();
        }).error(function(error) {
          $scope.errorList = error.errors;
          $scope.keys = null;
        });
      }
    });


  }

}])
