app.controller('RrpProjectCtrl', ['$scope','RrpProjectService','AuthenticationService','projectInfo','$state','$anchorScroll',function($scope,RrpProjectService,AuthenticationService,projectInfo,$state,$anchorScroll) {
  $scope.template="project-address.html";
 // $scope.orderCustomKeys = ['name','emailId','address1','userDTO','city','state','zipCode','homePhone'];

  $scope.occupantsList = ['Occupant not Owner','Premise Administator','Representative','Housing is not occupied','Owner Occupied','Other'];

  $scope.orderCustomKeys = ['occupantName','occupantType','address1','city','state','zipCode','homePhone','projectInformationDTO.pamphletDeliveryType','projectInformationDTO.deliveryDate','projectInformationDTO.projectName'];
  $scope.projectDTO = {};
  $scope.projectAddressDTO = {};
  $scope.projectInformationDTO = {};
  $scope.isOther = false;
  $scope.selected = "";
   $scope.hasChanged = function() {
        if($scope.projectAddressDTO.occupantType == 'Other'){
             $scope.isOther = true;
        }else{
            $scope.isOther = false;
        }
    }

  if(projectInfo && projectInfo.data){
    $scope.projectDTO =  projectInfo.data;
    $scope.projectAddressDTO = $scope.projectDTO.projectAddressDTO;
    $scope.projectInformationDTO =$scope.projectDTO.projectInformationDTO;

    if($scope.projectAddressDTO.occupantType && $scope.occupantsList.indexOf($scope.projectAddressDTO.occupantType) == -1){
        $scope.projectAddressDTO.otherOccupant = angular.copy($scope.projectAddressDTO.occupantType);
        $scope.projectAddressDTO.occupantType = 'Other';
        $scope.isOther = true;
    }else{
        $scope.isOther = false;
    }

    if($scope.projectAddressDTO.submissionDate){
        $scope.projectAddressDTO.submissionDate = new Date($scope.projectAddressDTO.submissionDate);
    }
    if($scope.projectInformationDTO.deliveryDate){
        $scope.projectInformationDTO.deliveryDate = new Date($scope.projectInformationDTO.deliveryDate);
    }

  }

  $scope.next = function(){
   $scope.errorList = null;
   $scope.keys = null;
   if($scope.isOther){
        $scope.projectAddressDTO.occupantType = $scope.projectAddressDTO.otherOccupant;
   }
    RrpProjectService.validateProjectAddress($scope.projectAddressDTO).success(function(response){
        $scope.template="project-information.html";
    }).error(function(error){
        if($scope.isOther && error.errors.occupantType){
            //JSON.parse('{"occupantType":[{"code":"NotBlank", "message":"Please enter Other Occupant Name"}]}')
           $scope.projectAddressDTO.occupantType = 'Other'
           error.errors.occupantType =  JSON.parse('[{"code":"NotBlank", "message":"Please enter Other Occupant Type"}]');
        }else if($scope.projectAddressDTO.occupantType &&  $scope.occupantsList.indexOf($scope.projectAddressDTO.occupantType) == -1){
              $scope.projectAddressDTO.otherOccupant = angular.copy($scope.projectAddressDTO.occupantType);
              $scope.projectAddressDTO.occupantType = 'Other';
              $scope.isOther = true;
           }
        $scope.errorList = error.errors;
        var temp = Object.keys($scope.errorList);
      $scope.keys =  [];
      for(var i = 0; i < $scope.orderCustomKeys.length ; i++) {
            if(temp.indexOf($scope.orderCustomKeys[i]) != -1){
                $scope.keys.push($scope.orderCustomKeys[i]);
            }
      }
      $anchorScroll();
    });
  }

  $scope.save = function (){
    $scope.projectAddressDTO.companyId = AuthenticationService.getCompanyId();
    $scope.projectDTO.projectAddressDTO =  $scope.projectAddressDTO ;
    $scope.projectDTO.projectInformationDTO= $scope.projectInformationDTO;
      $scope.errorList = null;
      $scope.keys = null;
     RrpProjectService.save($scope.projectDTO).success(function(response){
         if($scope.projectDTO.projectAddressDTO.id){
            swal('<h3>You have successfully updated project '+$scope.projectAddressDTO.occupantName+'</h3>')
         }else{
            swal('<h3>You have successfully created project '+$scope.projectAddressDTO.occupantName+'</h3>')
         }


         $state.go('app.projectList', {}, {reload: true});
    }).error(function(error){
      $scope.errorList = error.errors;
      var temp = Object.keys($scope.errorList);
            $scope.keys =  [];
            for(var i = 0; i < $scope.orderCustomKeys.length ; i++) {
                  if(temp.indexOf($scope.orderCustomKeys[i]) != -1){
                      $scope.keys.push($scope.orderCustomKeys[i]);
                  }
            }
      $anchorScroll();
    });
  }

  $scope.previous = function(){
      // TODO: Change path of template variable
      if($scope.projectAddressDTO.occupantType &&   $scope.occupantsList.indexOf($scope.projectAddressDTO.occupantType) == -1){
        $scope.projectAddressDTO.otherOccupant = angular.copy($scope.projectAddressDTO.occupantType);
        $scope.projectAddressDTO.occupantType = 'Other';
        $scope.isOther = true;
      }else{
        $scope.isOther = false;
      }
      $scope.template="project-address.html";
      $scope.errorList = null;
      $scope.keys =null;
    }

    $scope.cancel = function() {
        $state.go('app.projectList', {}, {reload: true});
    }


}]);
