app.controller('ResetPasswordCtrl',['$scope','$stateParams','$state','ChangePasswordService','AuthenticationService', function($scope,$stateParams,$state,ChangePasswordService,AuthenticationService) {

$scope.changePasswordDTO = {};

  $scope.forgotPassword = function(){
      if($stateParams.emailId){
        $scope.changePasswordDTO.emailId = $stateParams.emailId;
        $scope.changePasswordDTO.type = 'FORGOT';

         ChangePasswordService.forgotPassword($scope.changePasswordDTO).success(function(response){
              swal('<h3>Password reset successfully. Please login with new password.</h3>');
              $state.go('login', {}, {reload: true});
            }).error(function(error){
              $scope.errorList = error.errors;

              console.log("Error while reseting password");
            });
      }
  }

}])
