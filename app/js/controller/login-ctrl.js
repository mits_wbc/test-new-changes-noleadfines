app.controller('LoginCtrl',['$scope','$state','LoginService','AuthenticationService','$rootScope', function($scope,$state, LoginService,AuthenticationService,$rootScope) {

  $scope.userDetails = {};

  $scope.login = function() {

    $scope.errorList = null;
    $scope.keys = null;
    AuthenticationService.clearCache();

      console.log("LOGIN user: " + $scope.userDetails.email + " - PW: " + $scope.userDetails.password );
      LoginService.login($scope.userDetails).then(function (user){
        $scope.errorList = null;
        $scope.keys = null;
        if(user.errors){
          console.log("Login failed - Please check your credentials!");
          $scope.errorList = user.errors;
          $scope.keys = Object.keys($scope.errorList);
        }else {
          AuthenticationService.setToken(btoa($scope.userDetails.email +':'+$scope.userDetails.password));
          AuthenticationService.cacheUserDetails(user)
          AuthenticationService.cacheUserRoles(user);
          $state.go('app.team-members', {}, {reload: true});
      }
      });

  }

    $scope.gotoForgotPassword = function(){
      $state.go('forgot-password', {}, {reload: true});
    }

    $scope.goToCompanyRegistration = function(){
        AuthenticationService.clearCache();
        $state.go('company-information', {}, {reload: true});
    }



}])
