app.controller('SelectOptionCtrl', ['$scope','$state','AuthenticationService',function($scope,$state,AuthenticationService) {
    $scope.goToCompanyRegistration = function(){
           AuthenticationService.clearCache();
        $state.go('company-information', {}, {reload: true});
    }
}]);