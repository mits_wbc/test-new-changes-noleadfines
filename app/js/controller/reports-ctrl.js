app.controller('ReportsCtrl', ['$scope','$compile','ReportService',function($scope,$compile,ReportService) {
 $scope.$parent.deActiveLinks();
    $scope.$parent.isReport = true;

    $scope.isReport1 = true;
    $scope.isReport2 = false;
    $scope.isReport3 = false;



 ReportService.getReportBySkill().then(function (report){
        if(report.errors){
          $scope.errorList = error.errors;
          $scope.keys = Object.keys($scope.errorList);
        }else {
           $scope.projects = report;
            if(report.length>0){
                $scope.companyInfo = report[0];
            }

        }
 });

  $scope.template = "app/templates/skill-report.html";
//    $scope.template = "app/templates/area-report.html";

    $scope.report1 = function (){
        $scope.isReport1 = true;
        $scope.isReport2 = false;
        $scope.isReport3 = false;

         ReportService.getReportBySkill().then(function (report){
                if(report.errors){
                  $scope.errorList = error.errors;
                  $scope.keys = Object.keys($scope.errorList);
                }else {
                   $scope.projects = report;
                   if(report.length>0){
                                   $scope.companyInfo = report[0];
                               }
                }
         });
          $scope.template = "app/templates/skill-report.html";

    }

    $scope.report2 = function (){
        $scope.isReport2 = true;
        $scope.isReport1 = false;
        $scope.isReport3 = false;

        $scope.projects = {};
         ReportService.getReportByArea().then(function (report){
                if(report.errors){
                  $scope.errorList = error.errors;
                  $scope.keys = Object.keys($scope.errorList);
                }else {
                   $scope.projects = report;
                   if(report.length>0){
                                   $scope.companyInfo = report[0];
                               }
                }
           $scope.template = "app/templates/area-report.html";
         });

    }

    $scope.report3 = function (){
        $scope.isReport3 = true;
        $scope.isReport2 = false;
        $scope.isReport1 = false;

        $scope.projects = {};

         ReportService.reportBySkillAproval().then(function (report){
         $scope.template = "app/templates/skill-approval-report.html";
                if(report.errors){
                  $scope.errorList = error.errors;
                  $scope.keys = Object.keys($scope.errorList);
                }else {
                   $scope.projects = report;
                   if(report.length>0){
                       $scope.companyInfo = report[0];
                   }
                }

         });

    }

}]);