


//var APP_BASE_URL = 'http://localhost:8080/NWF';
var APP_BASE_URL = 'http://104.131.132.87:8080/NLF-API';
//var APP_BASE_URL = 'http://104.131.132.87/NLF-API';
//var APP_BASE_URL = 'http://72.44.225.140:8080/NLF-API'
// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
var app = angular.module("app", [
  'ngTouch',
  //'ngAnimate',
  'ui.router',
  'ui-notification',
  'chieffancypants.loadingBar',
  'ui.select',
  'ngSanitize',
  'uiSwitch',
  'ui.mask',
  'ngCookies'
]);

app.config( [ 'uiMask.ConfigProvider', function( uiMaskConfigProvider ) {
  uiMaskConfigProvider.clearOnBlur(true);
}]);

/**
 * Configuration for provide loader using angular-loading-bar.  '' use this
 */
app.config(function (cfpLoadingBarProvider) {
    cfpLoadingBarProvider.includeSpinner = true;

});

app.config(function ($httpProvider) {

    $httpProvider.interceptors.push('customeInterceptor');
    $httpProvider.defaults.useXDomain = true;
//    $httpProvider.defaults.withCredentials = true;
    delete $httpProvider.defaults.headers.common["X-Requested-With"];

});

// Custome Interceptor that will get injected on each HTTP request
// Add $ionicLoading at run time using $injector to avoid circular dependency error
    app.factory('customeInterceptor',['$timeout','$injector', '$q',function($timeout, $injector, $q) {


        return {
            request : function(config) {


                //console.log('Request Initiated with interceptor : ' + numLoadings);
                var token = localStorage.getItem('token');
                if(token){
                    config.headers['Authorization']= 'Basic '+ token;
                }
                 //config.headers['Authorization']= 'Basic '+ 'emJoYXR1a0BuYXZhZXJhLmNvbTpvYmppMTIzIw==';
                return config;
            },
            response : function(response) {

                //console.log('Response received with interceptor :' + numLoadings);

                return response;
            }
        }
    }]);
