app.factory('ReportService', ['$http','$q',function($http,$q){

    var reportService = {};

    reportService.getReportByArea = function () {

        var reportData = $q.defer();
            $http.get(APP_BASE_URL + '/secure/web/report/get-by-area').success(function(report){
             
                reportData.resolve(report);
              }).error(function(report){
                reportData.resolve(report);
              });
        return reportData.promise;
    }

    reportService.getReportBySkill = function () {
      var reportData = $q.defer();
        $http.get(APP_BASE_URL + '/secure/web/report/get-by-skill').success(function(report){
            reportData.resolve(report);
          }).error(function(report){
            reportData.resolve(report);
          });
        return reportData.promise;

    }

    reportService.reportBySkillAproval = function () {
      var reportData = $q.defer();
        $http.get(APP_BASE_URL + '/secure/web/report/get-by-skill-approval').success(function(report){
            reportData.resolve(report);
          }).error(function(report){
            reportData.resolve(report);
          });
        return reportData.promise;
    }

    return reportService;

}])