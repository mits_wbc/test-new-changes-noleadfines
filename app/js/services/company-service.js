app.factory('CompanyService', ['$http','$q','AuthenticationService',function($http,$q,AuthenticationService){
  var companyService = {};

  companyService.saveOrUpdate = function(companyDTO){
    if(companyDTO.id){
        return $http.post(APP_BASE_URL + '/secure/web/company/save-or-update',companyDTO);
    }

  }

  companyService.createCompany = function(companyDTO){
      return $http.post(APP_BASE_URL + '/web/company/save',companyDTO);
    }

  companyService.getCompanyById = function(){
    var companyId = AuthenticationService.getCompanyId();
    if(companyId){
         return $http.get(APP_BASE_URL + '/secure/web/company/get-by-id?companyId='+companyId);
    }
  }

  companyService.checkEPARegistration = function(){
      return $http.post(APP_BASE_URL + '/secure/web/company/check-epa-and-cr-number');
   }

  companyService.updateEPARegistration = function(companyDTO){
     return $http.post(APP_BASE_URL + '/secure/web/company/update-epa-and-cr-number',companyDTO);
  }

   return companyService;
 }]);