app.factory('RrpProjectService', ['$http','$q',function($http,$q){

   var rrpProjectService = {};

  rrpProjectService.getAllOccupantsWithDescription = function () {
    var occupantList = $q.defer();
    $http.get('app/js/json/occupants.json').success(function(data){
        occupantList.resolve(data);
    }).error(function(data){
         occupantList.resolve(data);
    });
        return occupantList.promise;
//    return $http.get('app/js/json/occupants.json');
  }

  rrpProjectService.getProjects = function(){
      // return $http.get(API_BASE_URL + '/json/project_list.json');
    // return $http.get(APP_BASE_URL + '/secure/project/getAllProject?companyId='+companyId);
    var projectsData= $q.defer();
    $http.get(APP_BASE_URL + '/secure/web/project/get-all-project').success(function(projects){
      projectsData.resolve(projects);
    }).error(function(projects){
      projectsData.resolve(projects);
    });
    return projectsData.promise;
  }

   rrpProjectService.validateProjectAddress = function (projectAddressDTO){
        return $http.post(APP_BASE_URL + '/secure/web/project/validate-project-address',projectAddressDTO);
   }

   rrpProjectService.validateProjectInformation = function (projectInformationDTO){
        return $http.post(APP_BASE_URL + '/secure/web/project/validate-project-information',projectInformationDTO);
   }

   rrpProjectService.save = function (projectDTO){
        return $http.post(APP_BASE_URL + '/secure/web/project/save-or-update',projectDTO);
   }

     rrpProjectService.getProjectById = function (projectId) {
       if(projectId != null){
         return $http.get(APP_BASE_URL + '/secure/web/project/get-project-by-id?projectId='+projectId);
       }
       return null;
     }

    rrpProjectService.delete = function(projectId){
      return $http.post(APP_BASE_URL + '/secure/web/project/delete?projectId='+projectId);
    }

   return rrpProjectService;
 }]);
