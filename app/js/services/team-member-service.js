/**
 * Created by vpatel on 7/6/2016.
 */

app.factory('TeamMemberService', ['$http','$q','AuthenticationService',function($http,$q,AuthenticationService){
  var teamMemberService = {};

  teamMemberService.getAllMembers = function(){
    var memberData = $q.defer();
    $http.get(API_BASE_URL + '/json/all_member_list.json').success(function(member){
      memberData.resolve(member);
    }).error(function(user){
      memberData.resolve(member);
    });
    return memberData.promise;
  }

  teamMemberService.addUserToProject = function (members,projectId) {
    return $http.post(APP_BASE_URL + '/secure/web/user/addUserToProject?projectId='+projectId,members);
  }

  teamMemberService.getAllTeamMembers = function(projectId){
    var memberData = $q.defer();
    $http.get(APP_BASE_URL + '/secure/web/user/getAllUserByProject?projectId='+projectId).success(function(members){
      memberData.resolve(members);
    }).error(function(user){
      memberData.resolve(members);
    });
    return memberData.promise;
  }

  teamMemberService.getAllCompanyMemberNotInProject = function(projectId){
    var memberData = $q.defer();
    var companyId = AuthenticationService.getCompanyId();
    $http.get(APP_BASE_URL + '/secure/web/user/getAllCompanyMemberNotInProject?projectId='+projectId+'&companyId='+companyId ).success(function(members){
      memberData.resolve(members);
    }).error(function(user){
      memberData.resolve(members);
    });
    return memberData.promise;
  }

  teamMemberService.getMemberById = function(memberId,projectId){
    if(memberId != null && projectId !=null){
      return $http.get(APP_BASE_URL + '/secure/web/user/getUserById?memberId='+memberId+'&projectId='+projectId);
    }else if(memberId != null && !projectId){
      return $http.get(APP_BASE_URL + '/secure/web/user/getUserById?memberId='+memberId);
    }

    /* if(memberId ==1){
      return $http.get(API_BASE_URL + '/json/team_member_1.json');
    }else if(memberId != null){
      return $http.get(API_BASE_URL + '/json/team_member_2.json');
    }*/
  }

  teamMemberService.saveOrUpdate = function (member) {
    return $http.post(APP_BASE_URL + '/secure/web/user/saveOrUpdate',member);
  }

  teamMemberService.changeExistingPassword = function(changePasswordDTO){
        return $http.post(APP_BASE_URL + '/secure/web/user/change-existing-password',changePasswordDTO);
  }

    teamMemberService.sendMailToEmployee = function(emailDTO,name){
          return $http.post(APP_BASE_URL + '/secure/web/user/send-mail-employee?userName='+name,emailDTO);
    }

    teamMemberService.getAllCompanyMembers = function () {
        var memberData = $q.defer();
        var companyId = AuthenticationService.getCompanyId();
        $http.get(APP_BASE_URL + '/secure/web/user/getAllUserByCompany?companyId='+companyId).success(function (member) {
          memberData.resolve(member);
        }).error(function (member) {
          memberData.resolve(member);
        });
        return memberData.promise;
     }

     teamMemberService.delete = function(memberId){
         return $http.post(APP_BASE_URL + '/secure/web/user/delete?userId='+memberId);
      }

  return teamMemberService;
}]);

app.filter('capitalize', function() {
    return function(input) {
      if (input !== null) {
        return input.replace(/\w\S*/g, function(txt) {
          return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        });
      }
    }
});
