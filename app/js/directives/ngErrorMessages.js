/**
 * Directive for handling server side validation messages.
 */
app.directive('ngErrorMessages', function ($compile) {
    return {
        restrict: 'EA',
        scope:{
            errorList: "=",
            keys: "="
        },
        template: "<div class='alert alert-danger' ng-if='errorList || keys.length >0'> <div  ng-repeat=\"key in keys\"><div ng-repeat='(k,val) in errorList[key]'><li> {{val.message}}</li></div></div></div>"
    }
});