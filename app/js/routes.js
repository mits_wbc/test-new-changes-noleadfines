
    app.config(
        ['$stateProvider', '$urlRouterProvider',
            function ($stateProvider, $urlRouterProvider) {
                //activation-report
                $urlRouterProvider
                    .otherwise('/login');

                $stateProvider
                    .state('app', {
                        abstract: true,
                        url: '/app',
                        controller:'AppCtrl',
                        templateUrl: 'app/templates/app.html'
                    })

                    .state('company-information', {
                        url: '/company-information',
                        templateUrl: 'app/templates/registration.html',
                        controller: 'CompanyInformationCtrl',
                        resolve: {
                         companyInfo: function ($stateParams, CompanyService) {
                          return null;
                         }
                        }
                    })

                    .state('app.company-information', {
                        url: '/company-information',
                        templateUrl: 'app/templates/company-information.html',
                        controller: 'CompanyInformationCtrl',
                        resolve: {
                        companyInfo: function ($stateParams, CompanyService) {
                          return CompanyService.getCompanyById();
                         }
                       }

                    })

                    .state('app.team-members', {
                        url: '/team-members',
                        cache: false,
                        templateUrl: 'app/templates/team-members.html',
                        controller: 'TeamMembersCtrl',
                        resolve: {
                          allMembers: function ($stateParams, TeamMemberService) {
                            return TeamMemberService.getAllCompanyMembers();
                          }
                        }
                    })

                    .state('app.update-member', {
                        url: '/update-member?memberId',
                        templateUrl: 'app/templates/update-member.html',
                        controller: 'UpdateMemberCtrl',
                        resolve: {
                            memberInfo: function ($stateParams, TeamMemberService) {
                              return TeamMemberService.getMemberById($stateParams.memberId,$stateParams.projectId);
                             }
                           }

                    })

                    .state('login', {
                        url: '/login',
                        templateUrl: 'app/templates/login.html',
                        controller: 'LoginCtrl'
                    })

                    .state('forgot-password', {
                        url: '/forgot-password',
                        templateUrl: 'app/templates/forgot-password.html',
                        controller: 'ForgotPasswordCtrl'
                    })
                    .state('reset-password', {
                        url: '/reset-password?emailId',
                        templateUrl: 'app/templates/reset-password.html',
                        controller: 'ResetPasswordCtrl'
                    })

                    .state('select-option', {
                        url: '/select-option',
                        templateUrl: 'app/templates/select-option.html',
                        controller: 'SelectOptionCtrl'
                    })

                    .state('app.RRP-project', {
                        url: '/RRP-project?projectId',
                        templateUrl: 'app/templates/rrp-project.html',
                        controller: 'RrpProjectCtrl',
                        resolve: {
                            projectInfo: function ($stateParams, RrpProjectService) {
                              return RrpProjectService.getProjectById($stateParams.projectId);
                             }
                          }
                    })

                     .state('app.projectList', {
                        url: '/get-all-project',
                        cache: false,
                        templateUrl: 'app/templates/project-list.html',
                        controller: 'ProjectListCtrl',
                        resolve: {
                            projectInfo: function ($stateParams, RrpProjectService) {
                              return RrpProjectService.getProjectById($stateParams.projectId);
                             }
                          }
                      })

                     .state('app.skillReport', {
                        url: '/skill-report',
                        cache: false,
                        templateUrl: 'app/templates/skill-report.html',
                        controller: 'ReportCtrl',
                        resolve: {
                            reportData: function (ReportService) {
                              return ReportService.getReportBySkill();
                             }
                          }
                      })

                     .state('app.areaReport', {
                        url: '/area-report',
                        cache: false,
                        templateUrl: 'app/templates/area-report.html',
                        controller: 'ReportCtrl',
                        resolve: {
                            reportData: function (ReportService) {
                              return ReportService.getReportByArea();
                             }
                          }
                      })

                     .state('app.skillApproval', {
                        url: '/skill-approval-report',
                        cache: false,
                        templateUrl: 'app/templates/skill-approval-report.html',
                        controller: 'ReportCtrl',
                        resolve: {
                            reportData: function (ReportService) {
                              return ReportService.reportBySkillAproval();
                             }
                          }
                      })

                       .state('app.reports', {
                                              url: '/reports',
                                              cache: false,
                                              templateUrl: 'app/templates/reports.html',
                                              controller: 'ReportsCtrl',

                                            })

                    .state('app.view4', {
                        url: '/view4',
                        templateUrl: 'app/templates/view4.html',
                        controller: 'View4Ctrl'

                    })

            }]);
